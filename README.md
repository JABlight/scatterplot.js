# scatterplot.js #

Prototype for an interactive scatterplot with HTML canvas.

You can select points on the canvas by using the mouse to lasso an area.