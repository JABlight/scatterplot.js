var 
    // Base canvas to draw points on
    baseCanvas = document.getElementById('base-canvas'),
    // Store the context for the base canvas
    baseContext = baseCanvas.getContext('2d'),
    // Canvas used to draw selection lasoo on
    drawCanvas = document.getElementById('draw-canvas'),
    // Store the context for the draw canvas
    drawContext = drawCanvas.getContext('2d'),
    // Canvas used to draw the selected points
    resultCanvas = document.getElementById('result-canvas'),
    // Store the context for the result canvas
    resultContext = resultCanvas.getContext('2d'),
    // The minimum distance to add another point to the selection polygon
    polyPointDistance = 15,
    // Stores the coordinates of the
    points = [],
    // Stores the coordinates and id of all points
    basePoints = [],
    // Stores the coordinates and id of selected points
    resultPoints = [],
    // Radius of each point
    pointRadius = 5;
    // The colour of the base points
    baseColour = '#CC78CE';
    // The colour of the selected points
    resultColour = '#F2570A',
    // Flag used to determine if selected has started
    started = false;


/**
  * Initialize the scatter plot
  */
function init () {
  
  // Draw some points on the base canvas
  drawBasePoints();  
  
  // Attach the mousemove event handler
  drawCanvas.addEventListener( 'mousedown', ev_mousedown, false );
}

init();

/**
  * Renders the initial points to the base canvas
  */
function drawBasePoints () {
  var centreX,
      centreY,
      canvasWidth = baseCanvas.width,
      canvasHeight = baseCanvas.height, 
      index,
      point;
  
  for (index = 0; index < 5000; index++) {
    centreX = Math.floor((Math.random() * canvasWidth) + 1 );
    centreY = Math.floor((Math.random() * canvasHeight) + 1 );
    point = {x: centreX, y: centreY};    
    
    drawPoint( baseContext, point, baseColour )   
    
    basePoints.push( point );
  }
}

/**
  * Renders the selected points to the results canvas
  */
function drawResultPoints () {
  var centreX,
      centreY,
      index,
      point;
  
  for (index = 0; index < resultPoints.length; index++) {
    centreX = resultPoints[index].x;
    centreY = resultPoints[index].y;
    point = {x: centreX, y: centreY};
    
    
    drawPoint( resultContext, point, resultColour );
  }  
}

/**
  * Renders a single point to a canvas.
  * @param context {Context} context to  draw on
  * @pos {*} x,y position to draw at
  * @color {String} color of the point to draw
  */
function drawPoint ( context, pos, color ) {  
  context.beginPath();
  context.arc( pos.x, pos.y, pointRadius, 0, 2 * Math.PI, false );
  context.lineWidth = 1;
  context.strokeStyle = color;
  context.stroke();  
}

/**
  * Determines whether or not a point lies within a polygon.
  * @param {Array} The points which make up the polygon
  * @pt {*} The point to test
  */
function isPointInPoly(poly, pt){
  for(var c = false, i = -1, l = poly.length, j = l - 1; ++i < l; j = i)
    ((poly[i].y <= pt.y && pt.y < poly[j].y) || (poly[j].y <= pt.y && pt.y < poly[i].y))
    && (pt.x < (poly[j].x - poly[i].x) * (pt.y - poly[i].y) / (poly[j].y - poly[i].y) + poly[i].x)
    && (c = !c);
  return c;
}

/**
  * Checks every point and adds it to the results array if it is contained in a polygon.
  * Then the points in the results array are rendered.
  * @param poly {Array} points which make up the polygon
  */
function pointsInPoly ( poly ) {
  var point,
      index;
  
  for ( index = 0; index < basePoints.length; index++) {
    point = basePoints[index];
    
    if ( isPointInPoly( poly, point ) ) {
      resultPoints.push( {x: point.x, y: point.y });
    }
  }
  
  // Draw the points
  drawResultPoints();
}

/**
  * Calvulates the distance between two points
  * @return {Number} 
  */
function lineDistance( point1, point2 )
{
  var xs = 0;
  var ys = 0;
 
  xs = point2.x - point1.x;
  xs = xs * xs;
 
  ys = point2.y - point1.y;
  ys = ys * ys;
 
  return Math.sqrt( xs + ys );
}


/**
  * Retrieves the mouse position
  * @param {Event} ev The mouse event
  * @return {*}
  */
function getMousePos (ev) {
  var x,y;
  
  // Get the mouse position relative to the <canvas> element
  if ( ev.layerX || ev.layerX === 0) {
    x = ev.layerX;
    y = ev.layerY;    
  } else if ( ev.offsetX || ev.offsetX === 0) {
    x = ev.offsetX;
    y = ev.offsetY;
  }
  
  return {x:x, y:y};
}


/**
  * Handler for the 'mousedown' event
  * Process the event by clearing the draw canvas, resetting the point array and
  * Adds the current current mouse position as the starting point for the polygon
  * Listeners are added for the mousemove and mouseup events
  * @param {Event} ev The 'mousedown' event
  */
function ev_mousedown (ev) {
  var pos = getMousePos(ev),
      x = pos.x,
      y = pos.y;
  
  if (!started) {
    // Clear the canvas
    drawContext.clearRect( 0, 0, drawCanvas.width, drawCanvas.height );
    resultContext.clearRect( 0, 0, drawCanvas.width, drawCanvas.height );

    // Clear any previous point and result array
    points = [];
    resultPoints = [];

    // first point
    drawContext.beginPath();
    drawContext.moveTo( x, y );

    points.push( { x:x, y:y } );

    // Attach the mousemove event handler
    drawCanvas.addEventListener( 'mousemove', ev_mousemove, false );
    // Attach the mouseup event handler
    drawCanvas.addEventListener( 'mouseup', ev_mouseup, false );
    
    started = true;
  }
}


/**
  * Handler for the 'mouseup' event
  * Process the event by completing the polygon and connecting the final point
  * Removes handlers for itself and 'mousemove' event
  * @param {Event} ev The event object
  */
function ev_mouseup (ev) {
  var firstPoint = points[0],
      x = firstPoint.x,
      y = firstPoint.y;
  
  // Make polygon by connecting last point to first
  drawContext.lineTo( x, y);
  drawContext.stroke();

  points.push( {x:x, y:y} );  
  
  // Remove the mousemove event handler
  drawCanvas.removeEventListener( 'mousemove', ev_mousemove, false ); 
  // Remove the mouseup event handler
  drawCanvas.removeEventListener( 'mouseup', ev_mouseup, false );
  
  // Check what points we have captured
  pointsInPoly (points);
  
  started = false;
}


/**
  * Handler for the 'mousedown' event
  * Process the event by checking the distance from the last point in the polygon and
  * adding another point if it is far enough away.
  * @param {Event} ev The event object
  */
function ev_mousemove ( ev ) {
  var pos =  getMousePos(ev),
      x = pos.x,
      y = pos.y;
  
  
  // The event handler works like a drawing pencil which
  //  tracks the mouse movements. We start dawing a path made up of lines
  if ( lineDistance( {x:x, y:y}, points[points.length-1]) >= polyPointDistance ) {    
    drawContext.strokeStyle = resultColour;
    drawContext.lineWidth = 4;
    drawContext.lineTo( x, y);
    drawContext.stroke();

    points.push( {x:x, y:y} );
  }
}